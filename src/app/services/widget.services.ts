import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Busqueda } from '../models/busqueda';

@Injectable()
export class WidgetService {

    constructor(private http: HttpClient) { }

    public getAirports(aeropuerto: string) {
        const URL = 'http://18.205.12.16:3333/airports?name=' + aeropuerto;
        const headers = new HttpHeaders();

        return this.http.get(URL, { headers, withCredentials: false });
    }

    public getAirports2() {
        const URL = 'http://18.205.12.16:3333/airports';
        const headers = new HttpHeaders();

        return this.http.get(URL, { headers, withCredentials: false });
    }

    public searchController(busqueda: Busqueda) {
        const URL = 'http://18.205.12.16:3333/search';
        const headers = new HttpHeaders();

        return this.http.post(URL, busqueda, { headers, withCredentials: false });
    }
}
