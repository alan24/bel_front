import { AeropuertosRelacionados } from '../models/aeropuertosRelacionados';
import { Aeropuerto } from 'src/app/models/aeropuerto';
import { Ciudad } from 'src/app/models/ciudad';

export default class AeropuertosMapper {
    static mapAeropuertos(respuestaServicio) {
        const aeropuerto: Aeropuerto = new Aeropuerto();
        const aeropuertosRelacionadosMap: AeropuertosRelacionados[] = [];

        aeropuerto.iata = respuestaServicio.iata;
        aeropuerto.aeropuertoID = respuestaServicio.idAirport;
        aeropuerto.nombre = respuestaServicio.name; // respuestaServicio.name;
        aeropuerto.texto = respuestaServicio.texto;
        aeropuerto.ciudad.nombre = respuestaServicio.city.name;

        if (respuestaServicio.relacionados !== null) {
            const listaRelacionados: any[] = respuestaServicio.relacionados;

            listaRelacionados.forEach(item => {
                const aeropuertoRelacionado = AeropuertosMapper.mapAeropuertosRelacionados(item);
                aeropuertosRelacionadosMap.push(aeropuertoRelacionado);
            });

            aeropuerto.aeropuertosRelacionados = aeropuertosRelacionadosMap;
        }
        return aeropuerto;
    }

    static mapAeropuertosRelacionados(aeropuerto) {
        const aeropuertoRelacionado: AeropuertosRelacionados = new AeropuertosRelacionados();

        aeropuertoRelacionado.aeropuertoID = aeropuerto.idAirport;
        aeropuertoRelacionado.iata = aeropuerto.iata;
        aeropuertoRelacionado.nombre = aeropuerto.name;
        aeropuertoRelacionado.ciudad.nombre = aeropuerto.city.name;

        return aeropuertoRelacionado;
    }
}
