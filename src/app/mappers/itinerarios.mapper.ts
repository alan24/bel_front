import { Itinerarios } from '../models/itinerarios';
import { Precio } from '../models/precio';
import { Segmentos } from '../models/segmentos';
import { Rutas } from '../models/rutas';

export default class ItinerariosMapper {
    static mapDatosItinerarios(respServicio: any) {
        const itinerario: Itinerarios = new Itinerarios();
        const segmentosMapeados: Segmentos[] = [];

        itinerario.moneda = respServicio.currency;
        itinerario.gdsType = respServicio.gdsType;
        itinerario.ticketTimeLimit = respServicio.ticketTimeLimit;
        itinerario.validatingCarrier = respServicio.validatingCarrier;
        // const precio: Precio = new Precio();
        itinerario.precio.baseFare = respServicio.price.baseFare;
        itinerario.precio.tax = respServicio.price.tax;
        itinerario.precio.total = respServicio.price.total;

        const itemSegmentos: any[] = respServicio.segments;
        itemSegmentos.forEach(item => {
            const segmento = ItinerariosMapper.mapSegmentos(item);
            segmentosMapeados.push(segmento);
        });

        itinerario.segmentos = segmentosMapeados;

        return itinerario;
    }

    static mapSegmentos(mapSegmento) {
        const segmento: Segmentos = new Segmentos();
        const rutasMapeadas: Rutas[] = [];

        segmento.destino = mapSegmento.destination;
        segmento.origen = mapSegmento.origin;

        const itemRutas: any[] = mapSegmento.routes;
        itemRutas.forEach(item => {
            const ruta = ItinerariosMapper.mapRutas(item);
            rutasMapeadas.push(ruta);
        });
        segmento.rutas = rutasMapeadas;

        return segmento;
    }

    static mapRutas(mapRutas) {
        const ruta: Rutas = new Rutas();

        ruta.fechaLlegada = mapRutas.arrivalDate;
        ruta.fechaSalida = mapRutas.departureDate;
        ruta.destino = mapRutas.destination;
        ruta.tiempoVuelo = mapRutas.flightTime;
        ruta.origen = mapRutas.origin;
        ruta.providerCode = mapRutas.providerCode;

        return ruta;
    }
}
