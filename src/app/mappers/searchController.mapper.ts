import { Busqueda } from 'src/app/models/busqueda';
import { WidgetComponent } from '../view/widget/widget.component';

export default class SearchControllerMapper {

    static mapDatosBusqueda(formGroup: any) {
        const datosBusqueda: Busqueda = new Busqueda();
        const search: SearchControllerMapper = new SearchControllerMapper();
        let tipoVuelo;
        let fSalida: string;
        let fRegreso: string;
        const idRandom = search.crearID();

        if (formGroup.tVuelo.value === 1) {
            tipoVuelo = 'REDONDO';
            fSalida = formGroup.fechaSalida.value;
            fRegreso = formGroup.fechaRegreso.value;
        } else if (formGroup.tVuelo.value === 2) {
            tipoVuelo = 'SENCILLO';
            fSalida = formGroup.fechaSalida.value;
            fRegreso = formGroup.fechaSalida.value;
        } else {
            tipoVuelo = 'MULTIDESTINO';
        }

        datosBusqueda.adults = parseInt(formGroup.pasajeros.value, 10);
        datosBusqueda.cabinPref = 'Economy';
        datosBusqueda.country = 'mexico';
        datosBusqueda.destination = formGroup.destino.value;
        datosBusqueda.destinationOJ = '';
        datosBusqueda.fhComback = fRegreso;
        datosBusqueda.fhDeparture = fSalida;
        datosBusqueda.flexFares = false;
        datosBusqueda.flightType = tipoVuelo;
        // datosBusqueda.maxStops = 0;
        datosBusqueda.origin = formGroup.salida.value;
        datosBusqueda.originOJ = '';
        datosBusqueda.sender = 'WEBMUNDOJOVEN';
        datosBusqueda.studentFare = false;
        datosBusqueda.id = idRandom;
        console.log('Mapper datos de la busqueda: ', datosBusqueda);

        return datosBusqueda;
    }

    crearID() {
        let text = '';
        const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

        for (let i = 0; i < 10; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }
}
