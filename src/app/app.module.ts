import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbDatepickerModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';

import { RoutingModule } from './view/widget/routing.module';

import { AppComponent } from './app.component';
import { WidgetComponent } from './view/widget/widget.component';
import { ReservacionComponent } from './view/reservacion/reservacion.component';

const rutas: Routes = [
  {
    path: '',
    component: WidgetComponent
  },
  {
    path: 'widget',
    component: WidgetComponent
  }
];
@NgModule({
  declarations: [
    AppComponent,
    WidgetComponent,
    ReservacionComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    NgbDatepickerModule,
    NgbTypeaheadModule,

    // Modulos http
    HttpClientModule,

    // Modulos de ruteo
    RouterModule.forRoot(rutas),
    RoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
