export class Busqueda {
    adults: number;
    ageChilds: number[];
    ageInfants: number[];
    airlineCode: string;
    cabinPref: string;
    childs: number;
    country: string;
    destination: string;
    destinationOJ: string;
    fhComback: string;
    fhDeparture: string;
    flexFares: boolean;
    flightType: string;
    id: string;
    infants: number;
    maxStops: number;
    metaClass: string;
    origin: string;
    originOJ: string;
    passengerCode: string;
    sender: string;
    studentFare: boolean;
}
