import { Segmentos } from './segmentos';
import { Precio } from './precio';

export class Itinerarios {
    moneda: string;
    gdsType: string;
    precio: Precio = new Precio();
    segmentos: Segmentos[] = [];
    ticketTimeLimit: Date;
    validatingCarrier: string;
}
