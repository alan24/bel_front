import { Pais } from './pais';

export class Ciudad {
    nombre: string;
    pais: Pais = new Pais();
}
