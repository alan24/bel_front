import { Ciudad } from './ciudad';

export class AeropuertosRelacionados {
    ciudad: Ciudad = new Ciudad();
    iata: string;
    aeropuertoID: number;
    nombre: string;
}
