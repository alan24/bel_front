import { Ciudad } from './ciudad';
import { AeropuertosRelacionados } from './aeropuertosRelacionados';

export class Aeropuerto {
    ciudad: Ciudad = new Ciudad();
    iata: string;
    aeropuertoID: number;
    nombre: string;
    texto: string;
    aeropuertosRelacionados: AeropuertosRelacionados[] = [];
}
