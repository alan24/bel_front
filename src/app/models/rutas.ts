export class Rutas {
    fechaLlegada: Date;
    fechaSalida: Date;
    destino: string;
    tiempoVuelo: string;
    origen: string;
    providerCode: string; // Aerolinea
}
