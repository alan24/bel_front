import { Rutas } from './rutas';

export class Segmentos {
    destino: string;
    id: string;
    key: string;
    origen: string;
    rutas: Rutas[] = [];
}
