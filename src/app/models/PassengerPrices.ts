export class PassengerPrices {
    baseFare: number;
    passengerType: string;
    serviceCharge: number;
    tax: number;
    total: number;
}
