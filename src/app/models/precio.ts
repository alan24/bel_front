import { PassengerPrices } from './PassengerPrices';

export class Precio {
    baseFare: number;
    serviceCharge: number;
    tax: number;
    total: number;
    passengerPrices: PassengerPrices[] = [];
}
