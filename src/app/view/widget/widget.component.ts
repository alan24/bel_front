import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { WidgetService } from '../../services/widget.services';
import { Itinerarios } from 'src/app/models/itinerarios';
import { Aeropuerto } from 'src/app/models/aeropuerto';
import SearchControllerMapper from '../../mappers/searchController.mapper';
import ItinerariosMapper from '../../mappers/itinerarios.mapper';
import AeropuertosMapper from '../../mappers/aeropuertos.mapper';

import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subject, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';
import { Ciudad } from 'src/app/models/ciudad';
// import * as moment from 'moment';


// import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/operator/map';

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.css'],
  providers: [
    WidgetService
  ]
})
export class WidgetComponent implements OnInit {
  private minDate: Date;
  private minDate2: Date;
  public opcionAvanzada = false;
  public formulario: FormGroup;
  public multidestino: FormGroup;
  public panelOpenState = false;
  public tipoVuelo: number;
  public itinerarios: Itinerarios[] = [];
  public aeropuertos: Aeropuerto[] = [];

  @ViewChild('instance') instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private widgetService: WidgetService) {

    /*this.minDate = moment().toDate();
    this.minDate2 = moment().add(1, 'd').toDate();*/
  }

  ngOnInit() {
    this.formulario = this.formBuilder.group({
      salida: ['', Validators.required],
      destino: ['', Validators.required],
      fechaSalida: ['', Validators.required],
      fechaRegreso: [{ value: '', disabled: true }, Validators.required],
      pasajeros: [1, Validators.required],
      tVuelo: [''],
      escalas: [''],
      clase: [''],
      codigo: [''],
      /*ocultoairport: [''],
      airport: [''],
      searchAirport: [''],*/
      multidestino: this.formBuilder.group({
        salidaM: [''],
        destinoM: [''],
        fechaSalidaM: [''],
        fechaRegresoM: [''],
        salidaM2: [''],
        destinoM2: [''],
        fechaSalidaM2: [''],
        fechaRegresoM2: [''],
        pasajerosM: [1, Validators.required],
      })
    });

    this.formulario.controls.tVuelo.setValue(2);

    this.widgetService.getAirports2().subscribe((res: any) => {
      console.log('Respuesta service getAirports: ', res);

      Object.values(res).forEach(element => {
        const aeropuerto = AeropuertosMapper.mapAeropuertos(element);
        this.aeropuertos.push(aeropuerto);
      });
      console.log('mapper de los aeropuertos: ', this.aeropuertos);
    });
  }

  buscar(): void {
    console.log('salida: ', this.formulario.controls.salida.value);
    console.log('destino: ', this.formulario.controls.destino.value);

    if (this.formulario.valid) {
      // this.validaFechas();
      this.itinerarios.splice(0);

      console.log('formulario completo: ', this.formulario.controls);
      /*this.widgetService.searchController(SearchControllerMapper.mapDatosBusqueda(this.formulario.controls)).subscribe(
        (respuesta: any) => {
          console.log('respuesta del servicio: ', respuesta.flightResponse.itineraries);

          respuesta.flightResponse.itineraries.forEach(element => {
            const itinerario = ItinerariosMapper.mapDatosItinerarios(element);
            this.itinerarios.push(itinerario);
          });
          console.log('mapper de los itinerarios: ', this.itinerarios);
        }
      );*/

    } else {
      console.log('El formulario no esta completo');
    }
  }

  irReservacion() {
    this.router.navigate(['/reservacion']);
  }

  changeAvanzada() {
    if (this.opcionAvanzada) {
      this.opcionAvanzada = false;
    } else {
      this.opcionAvanzada = true;
    }
  }

  changeForm() {
    if (this.formulario.controls.tVuelo.value === 1) {
      this.tipoVuelo = 1;
      console.log('formulario 1');
      this.formulario.controls.salida.enable();
      this.formulario.controls.destino.enable();
      this.formulario.controls.fechaSalida.enable();
      this.formulario.controls.fechaRegreso.enable();
    } else if (this.formulario.controls.tVuelo.value === 2) {
      console.log('formulario 2');
      this.tipoVuelo = 2;
      this.formulario.controls.salida.enable();
      this.formulario.controls.destino.enable();
      this.formulario.controls.fechaSalida.enable();
      this.formulario.controls.fechaRegreso.disable();
    } else if (this.formulario.controls.tVuelo.value === 3) {
      console.log('formulario 3');
      this.tipoVuelo = 3;
      this.formulario.controls.salida.disable();
      this.formulario.controls.destino.disable();
      this.formulario.controls.fechaSalida.disable();
      this.formulario.controls.fechaRegreso.disable();
    }
  }

  /*validaFechaRegreso() {
    if (this.tipoVuelo === 1) {
      this.minDate2 = moment(this.formulario.controls.fechaSalida.value).add(1, 'd').toDate();
      console.log('calcular fecha de regreso: ', this.minDate2);
    }

    this.minDate2 = moment(this.formulario.controls.fechaSalida.value).add(1, 'd').toDate();
    this.formulario.controls.fechaRegreso.setValue(this.minDate2);
    console.log('calcular fecha de regreso: ', this.minDate2);
  }

  validaFechas() {
    let fSalida: string;
    let fRegreso: string;

    fSalida = moment(this.formulario.controls.fechaSalida.value).format('DD-MM-YYYY');
    fRegreso = moment(this.formulario.controls.fechaRegreso.value).format('DD-MM-YYYY');

    this.formulario.controls.fechaSalida.setValue(fSalida);
    this.formulario.controls.fechaRegreso.setValue(fRegreso);
  }*/


  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.aeropuertos.filter(v => v.texto.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )

  formatter = (x: {texto: string, iata: string }) => x.texto;
  // formatter = (x: { ciudad: Ciudad, iata: string }) =>  x.ciudad.nombre + ' ' + x.iata ;
}
