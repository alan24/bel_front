import { RouterModule, Routes } from '@angular/router';
import { WidgetComponent } from './widget.component';
import { ReservacionComponent } from '../reservacion/reservacion.component';

const routes: Routes = [
  {
    path: 'reservacion',
    component: ReservacionComponent
  }
];

export const RoutingModule = RouterModule.forChild(routes);
