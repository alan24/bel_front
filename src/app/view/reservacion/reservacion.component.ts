import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-reservacion',
  templateUrl: './reservacion.component.html',
  styleUrls: ['./reservacion.component.css']
})
export class ReservacionComponent implements OnInit {
  public formulario: FormGroup;
  public totalCompra: number;
  public panelOpenState = false;

  constructor(
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.totalCompra = 50000;
    this.formulario = this.formBuilder.group({
      nombre: ['', Validators.required],
      apellidos: ['', Validators.required],
      nacionalidad: ['', Validators.required],
      numPasaporte: ['', Validators.required],
      fechaNacimiento: ['', Validators.required],
      sexo: [''],
      telefono: [''],
      correo: ['']
    });
  }

  continuar() {

  }

}
